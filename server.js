"use strict";
var cluster = require('cluster');//Включаем cluster
var events = require('events');
var os = require('os');
var cpuCount = 1;//require('os').cpus().length;//Количество ядер процессора(потоков)
//В мастере создаем worker'ов равное количеству ядер процессоров
if (cluster.isMaster) {
    var workers = [];
    var workersInfo = [];
    var arrayPortWorkers = [];
    var infoCPU = [];
    for (var i = 0; i < cpuCount; i++) {
        cluster.setupMaster({
            args: [(5001 + i)]
        });
        workers[i] = cluster.fork();
        arrayPortWorkers.push(':' + (5001 + i));
        workers[i].on('message', function (msg) {
            if (msg.type === "answerServerInfo") {
                var flag = 0;
                for (var i = workersInfo.length; i--;) {
                    if (workersInfo[i] !== undefined) {
                        if (workersInfo[i].id === msg.id) {
                            workersInfo[i].pid = msg.pid;
                            workersInfo[i].port = msg.port;
                            workersInfo[i].useMemory = msg.useMemory;
                            workersInfo[i].allocatedMemory = msg.allocatedMemory;
                            workersInfo[i].countUser = msg.countUser;
                            flag = 1;
                            break;
                        }
                    }
                }
                if (flag === 0) workersInfo.push({
                    id: msg.id,
                    pid: msg.pid,
                    port: msg.port,
                    useMemory: msg.useMemory,
                    allocatedMemory: msg.allocatedMemory,
                    countUser: msg.countUser
                });
            }

        });
    }
    /**---------Баласер подключений-------**/

    var compressionBalancer = require('compression');
    var expressBalancer = require('express');
    var appBalancer = expressBalancer();
    appBalancer.use(compressionBalancer());
    var httpBalancer = require('http').Server(appBalancer);
    var ioBalancer = require('socket.io')(httpBalancer);
    var NumHost = 0;
    appBalancer.get("/", function (request, response) {
        NumHost++;
        if (NumHost === cpuCount) NumHost = 0;
        response.redirect("http://" + request.headers.host.split(':')[0] + arrayPortWorkers[NumHost]);
    });
    appBalancer.get("/monitor", function (request, response) {
        response.sendFile(__dirname + '/monitorServer.html');
    });

    appBalancer.set('port', (process.env.PORT || 5000));
    httpBalancer.listen(appBalancer.get('port'), function () {
        console.log('Запущен балансер на порту', appBalancer.get('port'));
    });

    ioBalancer.on('connection', function (socket) {
        /** Получение состояние сервера для мониторинга**/
        socket.on('serverInfo', function () {

            var used = process.memoryUsage().heapUsed / 1024 / 1024;
            socket.emit('answerServerInfo', Math.round(used * 100) / 100, 0, cpuCount, workersInfo, infoCPU);
        });
    });
    /**-----------------------------------**/
    cluster.on('exit', function (worker, code, signal) {
        console.log('worker ' + worker.id + ' died');
        for (var i = workers.length; i--;) {
            if (workers[i].id === worker.id) {
                for (var j = workersInfo.length; j--;) {
                    if (workersInfo[j].id === workers[i].id) {
                        cluster.setupMaster({
                            args: [workersInfo[j].port]
                        });
                        var newWorker = cluster.fork();
                        newWorker.on('message', function (msg) {
                            if (msg.type === "answerServerInfo") {
                                var flag = 0;
                                for (var i = workersInfo.length; i--;) {
                                    if (workersInfo[i] !== undefined) {
                                        if (workersInfo[i].id === msg.id) {
                                            workersInfo[i].pid = msg.pid;
                                            workersInfo[i].port = msg.port;
                                            workersInfo[i].useMemory = msg.useMemory;
                                            workersInfo[i].allocatedMemory = msg.allocatedMemory;
                                            workersInfo[i].countUser = msg.countUser;
                                            flag = 1;
                                            break;
                                        }
                                    }
                                }
                                if (flag === 0) workersInfo.push({
                                    id: msg.id,
                                    pid: msg.pid,
                                    port: msg.port,
                                    useMemory: msg.useMemory,
                                    allocatedMemory: msg.allocatedMemory,
                                    countUser: msg.countUser
                                });
                            }

                        });
                        workers.push(newWorker);
                        workersInfo.splice(j, 1);
                    }
                }
                workers.splice(i, 1);
                break;
            }
        }
    });
    var cpus = os.cpus();
    var lastcpus = 0;
    setInterval(function () {
        for (const id in cluster.workers) {
            cluster.workers[id].send({type: 'GetServerInfo'});
        }
        infoCPU.length = 0;
        if (lastcpus !== 0) {
            cpus = os.cpus();
            //console.log("freemem",Math.round(os.freemem() / 1024 / 1024 * 100) / 100);
            //console.log("totalmem",Math.round(os.totalmem() / 1024 / 1024 * 100) / 100);

            for (var i = 0, len = cpus.length; i < len; i++) {
                var lcpu = lastcpus[i], cpu = cpus[i], total = 0;
                for (var type in cpu.times) {
                    total += cpu.times[type] - lcpu.times[type];
                }
                infoCPU.push({
                    numberCPU: "CPU" + i, userCPU: Math.round(100 * (cpu.times.user - lcpu.times.user) / total),
                    sysCPU: Math.round(100 * (cpu.times.sys - lcpu.times.sys) / total),
                    idleCPU: Math.round(100 * (cpu.times.idle - lcpu.times.idle) / total)
                });
            }
        }
        lastcpus = os.cpus();
    }, 1000);
}

if (cluster.isWorker) {
    var worker_id = cluster.worker.id;
    var port = process.argv[2];
    console.log("Worker id:", worker_id, "pid:", process.pid, "started on port:", port);
    var compression = require('compression');
    var express = require('express'); // Express contains some boilerplate to for routing and such
    var app = express();
    var cookieParser = require('cookie-parser');
    var http = require('http').Server(app);
    var io = require('socket.io')(http); // Here's where we include socket.io as a node module
    var uuid = require('uuid');
    var cookie = require('cookie');
    var AI = require('./AI.js');
    AI.load();
    const MongoClient = require("mongodb").MongoClient;
    app.use(compression({threshold: 1}));
    app.use(cookieParser());
    app.use('/assets', express.static('assets'));
    var localDB = [];
    var countUser = 0;

    process.on('message', function (msg) {
        // we only want to intercept messages that have a chat property
        if (msg.type === 'GetServerInfo') {
            process.send({
                type: "answerServerInfo", id: worker_id,
                pid: process.pid,
                port: port,
                useMemory: Math.round(process.memoryUsage().heapTotal / 1024 / 1024 * 100) / 100,
                allocatedMemory: Math.round(process.memoryUsage().rss / 1024 / 1024 * 100) / 100,
                countUser: countUser
            });
        }
    });

    app.use(function (req, res, next) {
        var cookie = req.cookies.cookieName;
        if (cookie === undefined) {
            var idUser = uuid.v4();
            res.cookie('cookieName', idUser, {maxAge: 900000, httpOnly: true});
            console.log('Создана новая cookie ' + idUser);
        }
        else {
            console.log('У пользователя есть cookie', cookie);
        }
        next();
    });

    /**Маршрутизация**/
    app.get("/", function (request, response) {
        response.sendFile(__dirname + '/index.html');
    });

    app.get("/monitor", function (request, response) {
        response.sendFile(__dirname + '/monitorGame.html');
    });

    app.get("/three", function (request, response) {
        response.sendFile(__dirname + '/index3D.html');
    });

    app.use('/resources', express.static(__dirname + '/resources'));

// Listen on port 5000
    app.set('port', (process.env.PORT || process.argv[2]));

    /*    app.listen(process.argv[2], function () {
            console.log('listening on port', app.get('port'));
        });*/
    http.listen(app.get('port'), function () {
        console.log('listening on port', app.get('port'));
    });

    io.on('connection', function (socket) {
        countUser = io.engine.clientsCount;
        socket.on('game', function (isNewGame, UserCode) {

            if (socket.request.headers.cookie === undefined) {
                socket.emit('redirect', '/');
            } else {
                var cookie1 = cookie.parse(socket.request.headers.cookie);
                //var idUser = cookie1['cookieName'];
                var eventEmitter = new events.EventEmitter();
                const mongoClient = new MongoClient("mongodb://localhost:27017/", {useNewUrlParser: true});
                eventEmitter.on('GameLoad', function () {
                    //socket.join(socket.id);
                    var numGame = findGame(UserCode);
                    socket.emit('stateGame',
                        localDB[numGame].stateGame,
                        localDB[numGame].whoGo,
                        localDB[numGame].PlayerPosX,
                        localDB[numGame].PlayerPosY,
                        localDB[numGame].PlayerPos2X,
                        localDB[numGame].PlayerPos2Y,
                        localDB[numGame].step,
                        localDB[numGame].numRedCell,
                        localDB[numGame].numBlueCell,
                        countUser);
                });
                mongoClient.connect(function (err, client) {
                    const db = client.db("usersdb");
                    if (err) return console.log(err);
                    db.collection("games").findOne({idUser: UserCode}, function (err, doc) {
                        var idGame = uuid.v4();
                        if (doc !== null) {
                            var numGame = findGame(UserCode);
                            if (numGame !== undefined) {
                                if (isNewGame === 1) {
                                    var table = [];
                                    for (var i = 15; i--;) {
                                        table[i] = [];
                                        table[i].push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                                    }
                                    table[0][0] = 3;
                                    table[14][14] = 4;
                                    localDB[numGame].stateGame = table;
                                    localDB[numGame].whoGo = 1;
                                    localDB[numGame].PlayerPosX = 0;
                                    localDB[numGame].PlayerPosY = 0;
                                    localDB[numGame].PlayerPos2X = 14;
                                    localDB[numGame].PlayerPos2Y = 14;
                                    localDB[numGame].step = 0;
                                    localDB[numGame].numRedCell = 1;
                                    localDB[numGame].numBlueCell = 1;
                                } else {
                                    localDB[numGame].idGame = doc.idGame;
                                    localDB[numGame].stateGame = doc.stateGame;
                                    localDB[numGame].whoGo = doc.whoGo;
                                    localDB[numGame].PlayerPosX = doc.PlayerPosX;
                                    localDB[numGame].PlayerPosY = doc.PlayerPosY;
                                    localDB[numGame].PlayerPos2X = doc.PlayerPos2X;
                                    localDB[numGame].PlayerPos2Y = doc.PlayerPos2Y;
                                    localDB[numGame].step = doc.step;
                                    localDB[numGame].numRedCell = doc.numRedCell;
                                    localDB[numGame].numBlueCell = doc.numBlueCell;
                                }
                            } else {
                                if (isNewGame === 1) {
                                    var table = [];
                                    for (var i = 15; i--;) {
                                        table[i] = [];
                                        table[i].push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                                    }
                                    table[0][0] = 3;
                                    table[14][14] = 4;
                                    localDB.push({
                                        idGame: doc.idGame,
                                        idUser: doc.idUser,
                                        stateGame: table,
                                        whoGo: 1,
                                        PlayerPosX: 0,
                                        PlayerPosY: 0,
                                        PlayerPos2X: 1,
                                        PlayerPos2Y: 1,
                                        step: 0,
                                        numRedCell: 1,
                                        numBlueCell: 1
                                    });
                                } else {
                                    localDB.push({
                                        idGame: doc.idGame,
                                        idUser: doc.idUser,
                                        stateGame: doc.stateGame,
                                        whoGo: doc.whoGo,
                                        PlayerPosX: doc.PlayerPosX,
                                        PlayerPosY: doc.PlayerPosY,
                                        PlayerPos2X: doc.PlayerPos2X,
                                        PlayerPos2Y: doc.PlayerPos2Y,
                                        step: doc.step,
                                        numRedCell: doc.numRedCell,
                                        numBlueCell: doc.numBlueCell
                                    });
                                }
                            }
                            console.log("Игра найдена, состояние загружено для игрока: " + UserCode);
                            eventEmitter.emit('GameLoad');
                        } else {
                            var table = [];
                            for (var i = 15; i--;) {
                                table[i] = [];
                                table[i].push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                            }
                            table[0][0] = 3;
                            table[14][14] = 4;
                            db.collection("games").insertOne({
                                idGame: idGame,
                                idUser: UserCode,
                                stateGame: table,
                                whoGo: 1,
                                PlayerPosX: 0,
                                PlayerPosY: 0,
                                PlayerPos2X: 14,
                                PlayerPos2Y: 14,
                                step: 0,
                                numRedCell: 1,
                                numBlueCell: 1
                            }, function (err, results) {
                                if (err) return console.log(err);
                                client.close();
                                var numGame = findGame(UserCode, localDB);
                                if (numGame !== undefined) {
                                    localDB[numGame].idGame = idGame;
                                    localDB[numGame].stateGame = table;
                                    localDB[numGame].whoGo = 1;
                                    localDB[numGame].PlayerPosX = 0;
                                    localDB[numGame].PlayerPosY = 0;
                                    localDB[numGame].PlayerPos2X = 14;
                                    localDB[numGame].PlayerPos2Y = 14;
                                    localDB[numGame].step = 0;
                                    localDB[numGame].numRedCell = 1;
                                    localDB[numGame].numBlueCell = 1;
                                    console.log("Игра найдена, перезаписана для игрока: " + UserCode);
                                } else {
                                    localDB.push({
                                        idGame: idGame,
                                        idUser: UserCode,
                                        stateGame: table,
                                        whoGo: 1,
                                        PlayerPosX: 0,
                                        PlayerPosY: 0,
                                        PlayerPos2X: 14,
                                        PlayerPos2Y: 14,
                                        step: 0,
                                        numRedCell: 1,
                                        numBlueCell: 1
                                    });
                                    console.log("Игра не найдена, создана новая для игрока: " + UserCode);
                                }
                                eventEmitter.emit('GameLoad');
                            });
                        }
                        if (err) return console.log(err);
                        client.close();
                    });
                });
            }
        });

        // Listen for a disconnection and update our player table
        socket.on('disconnect', function (state) {
            console.log("disconnect_cookie");

            if (socket.request.headers.cookie !== undefined) {
                var cookie1 = cookie.parse(socket.request.headers.cookie);
                var b = cookie1['cookieName'];
                var numGame = findGame(b, localDB);
                if (numGame !== undefined) {
                    UpdateGameState(b, localDB[numGame]);
                    //localDB.splice(numGame,1);
                }
            }
            countUser = io.engine.clientsCount;
        });
        /** Получение состояние сервера для мониторинга**/
        socket.on('serverInfo', function () {
            var used = process.memoryUsage().heapUsed / 1024 / 1024;
            socket.emit('answerServerInfo', Math.round(used * 100) / 100, countUser);
        });
        /** Получение состояние игр для мониторинга**/
        socket.on('getLocalDB', function () {
            socket.emit('answerLocalDB', localDB);
        });
        socket.on('move', function (direction, UserCode) {
            /** Логирование памяти в консоль**/
            /*
            var used = process.memoryUsage().heapUsed / 1024 / 1024;
            console.log("The script uses " + Math.round(used * 100) / 100 + " MB");
            */
            var cookie1 = cookie.parse(socket.request.headers.cookie);
            var b = cookie1['cookieName'];
            var numGame = findGame(UserCode, localDB);
            if (localDB[numGame] !== undefined) {
                switch (direction) {
                    case 'w':
                        if (userCanGo(localDB[numGame].PlayerPos2X - 1, localDB[numGame].PlayerPos2Y, localDB[numGame]) === 1) {
                            localDB[numGame].stateGame[localDB[numGame].PlayerPos2X][localDB[numGame].PlayerPos2Y] = 2;
                            localDB[numGame].PlayerPos2X--;
                            localDB[numGame].stateGame[localDB[numGame].PlayerPos2X][localDB[numGame].PlayerPos2Y] = 4;
                        }
                        break;
                    case 's':
                        if (userCanGo(localDB[numGame].PlayerPos2X + 1, localDB[numGame].PlayerPos2Y, localDB[numGame]) === 1) {
                            localDB[numGame].stateGame[localDB[numGame].PlayerPos2X][localDB[numGame].PlayerPos2Y] = 2;
                            localDB[numGame].PlayerPos2X++;
                            localDB[numGame].stateGame[localDB[numGame].PlayerPos2X][localDB[numGame].PlayerPos2Y] = 4;
                        }
                        break;
                    case 'a':
                        if (userCanGo(localDB[numGame].PlayerPos2X, localDB[numGame].PlayerPos2Y - 1, localDB[numGame]) === 1) {
                            localDB[numGame].stateGame[localDB[numGame].PlayerPos2X][localDB[numGame].PlayerPos2Y] = 2;
                            localDB[numGame].PlayerPos2Y--;
                            localDB[numGame].stateGame[localDB[numGame].PlayerPos2X][localDB[numGame].PlayerPos2Y] = 4;
                        }
                        break;
                    case 'd':
                        if (userCanGo(localDB[numGame].PlayerPos2X, localDB[numGame].PlayerPos2Y + 1, localDB[numGame]) === 1) {
                            localDB[numGame].stateGame[localDB[numGame].PlayerPos2X][localDB[numGame].PlayerPos2Y] = 2;
                            localDB[numGame].PlayerPos2Y++;
                            localDB[numGame].stateGame[localDB[numGame].PlayerPos2X][localDB[numGame].PlayerPos2Y] = 4;
                        }
                        break;
                    default:
                }
                localDB[numGame].step++;
                AI.makeMove(localDB[numGame]);
                checkAreaClosurePlayer(localDB[numGame], 2, 1, 3);
                checkAreaClosurePlayer(localDB[numGame], 1, 2, 4);
                countCell(localDB[numGame]);
                var answer;
                if ((localDB[numGame].numRedCell + localDB[numGame].numBlueCell === 225) || localDB[numGame].step === 500) {
                    answer = 'finalGame';
                } else {
                    answer = 'stateGame'
                }
                socket.emit(answer,
                    localDB[numGame].stateGame,
                    localDB[numGame].whoGo,
                    localDB[numGame].PlayerPosX,
                    localDB[numGame].PlayerPosY,
                    localDB[numGame].PlayerPos2X,
                    localDB[numGame].PlayerPos2Y,
                    localDB[numGame].step,
                    localDB[numGame].numRedCell,
                    localDB[numGame].numBlueCell,
                    countUser);
            }
        });
    });
}

/**Поиск игры по idUser в localDB**/
function findGame(idUser) {
    for (var i = localDB.length; i--;) {
        if (localDB[i].idUser.localeCompare(idUser) === 0) {
            return i;
        }
    }
}

/**Обновление состояния игры в mongoDB**/
function UpdateGameState(idUser, game) {
    if (game !== undefined) {
        const mongoClient = new MongoClient("mongodb://localhost:27017/", {useNewUrlParser: true});
        mongoClient.connect(function (err, client) {
            if (err) return console.log(err);
            const db = client.db("usersdb");
            db.collection("games").findOneAndUpdate(
                {idUser: idUser},              // критерий выборки
                {
                    $set: {
                        stateGame: game.stateGame,
                        whoGo: game.whoGo,
                        PlayerPosX: game.PlayerPosX,
                        PlayerPosY: game.PlayerPosY,
                        PlayerPos2X: game.PlayerPos2X,
                        PlayerPos2Y: game.PlayerPos2Y,
                        step: game.step,
                        numRedCell: game.numRedCell,
                        numBlueCell: game.numBlueCell
                    }
                },     // параметр обновления
                {                           // доп. опции обновления
                    returnOriginal: false
                },
                function (err, result) {
                    //console.log(result);
                    client.close();
                }
            );
        });
    } else {
        console.log("Ошибка в обновлении БД, нет обЪекта game")
    }
}

/**Проверка возможности хода для пользователя**/
function userCanGo(x, y, game) {
    if ((x > -1 && x < 15) && (y > -1 && y < 15) && (game.stateGame[x][y] === 0 || game.stateGame[x][y] === 2)) return 1;
    return 0;
}

/**Подсчет количества закрашенных клеток**/
function countCell(game) {
    var numRedCell = 0, numBlueCell = 0;
    for (var i = 15; i--;) {
        for (var j = 15; j--;) {
            switch (game.stateGame[i][j]) {
                case 1:
                    numRedCell++;
                    break;
                case 2:
                    numBlueCell++;
                    break;
                case 3:
                    numRedCell++;
                    break;
                case 4:
                    numBlueCell++;
                    break;
            }
        }
    }
    game.numRedCell = numRedCell;
    game.numBlueCell = numBlueCell;
}

/**Проверка замыкания области**/
function checkAreaClosurePlayer(game, typeCell, typeCellEnemy, typeEnemy) {
    var copyStateGame = new Array(15);
    var whiteCell = [];
    for (var i = 0; i < copyStateGame.length; i++) {
        copyStateGame[i] = new Array(15);
        for (var j = 0; j < 15; j++) {
            copyStateGame[i][j] = game.stateGame[i][j];
            if (copyStateGame[i][j] === 0) {
                whiteCell.push([i,j]);
            }
        }
    }
    var repaintCell = true;
    while (repaintCell) {
        repaintCell = false;
        whiteCell.forEach(function (item, i, arr) {
            if (checkCell(copyStateGame, item, typeCellEnemy, typeEnemy)) {
                arr.splice(i, 1);
                repaintCell = true;
            }
        });
    }
    whiteCell.forEach(function (item, i, arr) {
        game.stateGame[item[0]][item[1]] = typeCell;
    });
}

function checkCell(copyStateGame, item, typeCellEnemy, typeEnemy) {
    var localTypeCellEnemy  = typeCellEnemy;
    var localTypeEnemy  = typeEnemy;
    for (var i = -1; i < 2; i++) {
        for (var j = -1; j < 2; j++) {
            if (item[0] + i > -1 && item[0] + i < 15 && item[1] + j > -1 && item[1] + j < 15) {
                if (copyStateGame[item[0] + i][item[1] + j] === localTypeCellEnemy || copyStateGame[item[0] + i][item[1] + j] === -1 || copyStateGame[item[0] + i][item[1] + j] === localTypeEnemy) {
                    copyStateGame[item[0]][item[1]] = -1;
                    return true;
                }
            } else {
                copyStateGame[item[0]][item[1]] = -1;
                return true;
            }
        }
    }
    return false;
}