var express = require('express'); // Express contains some boilerplate to for routing and such
var app = express();
var cookieParser = require('cookie-parser')
var events = require('events');
var http = require('http').Server(app);
var io = require('socket.io')(http); // Here's where we include socket.io as a node module
var uuid = require('uuid');
var cookie = require('cookie');
const MongoClient = require("mongodb").MongoClient;

var table = new Array(15);		// В таблице 10 строк

for(var q = 0; q < table.length; q++)
    table[q] = new Array(15);

for(var row = 0; row < table.length; row++) {
    for(var col = 0; col < table[row].length; col++) {
        table[row][col] = 0;
    }
}
table[0][0] = 3;
table[14][14] =4;

var localBD=[];
app.use(cookieParser());

app.use(function (req, res, next) {
    var eventEmitter = new events.EventEmitter();
    //res.locals.error = req.app.get('env') === 'development' ? err : {};
    // check if client sent cookie
    var cookie = req.cookies.cookieName;
    const mongoClient = new MongoClient("mongodb://localhost:27017/", { useNewUrlParser: true });
    var idGame=uuid.v4();
    var idUser=uuid.v4();
    eventEmitter.on('GameLoad', function(){
        next();
    });
    if (cookie === undefined)
    {
        // no: set a new cookie
        mongoClient.connect(function(err, client){
            const db = client.db("usersdb");
            if(err) return console.log(err);
            db.collection("games").insertOne({idGame: idGame,idUser: idUser, stateGame: table, whoGo: 1,PlayerPosX:0,PlayerPosY:0,PlayerPos2X:14,PlayerPos2Y:14} , function(err, results){
                console.log("Добавлена игра");

                if(err) return console.log(err);
                client.close();
            });
        });
        localBD.push({idGame: idGame,idUser: idUser, stateGame: table, whoGo: 1,PlayerPosX:0,PlayerPosY:0,PlayerPos2X:14,PlayerPos2Y:14});
        res.cookie('cookieName',idUser, { maxAge: 900000, httpOnly: true });
        console.log('Создана новая cookie '+idUser);
        eventEmitter.emit('GameLoad');
    }
    else
    {
        var flag=true;
        mongoClient.connect(function(err, client){

            const db = client.db("usersdb");
            if(err) return console.log(err);
            db.collection("games").findOne({idUser: cookie}, function(err, doc){
                //console.log(doc);
                if(doc!==null){
                    localBD.push({idGame: doc.idGame,
                        idUser: doc.idUser,
                        stateGame: doc.stateGame,
                        whoGo: doc.whoGo,
                        PlayerPosX:doc.PlayerPosX,
                        PlayerPosY:doc.PlayerPosY,
                        PlayerPos2X:doc.PlayerPos2X,
                        PlayerPos2Y:doc.PlayerPos2Y});
                        console.log("Игра найдена, состояние загружено");
                        eventEmitter.emit('GameLoad');
                }else{
                    db.collection("games").insertOne({idGame: idGame,idUser: cookie, stateGame: table, whoGo: 1,PlayerPosX:0,PlayerPosY:0,PlayerPos2X:14,PlayerPos2Y:14} , function(err, results){
                        localBD.push({idGame: idGame,idUser: cookie, stateGame: table, whoGo: 1,PlayerPosX:0,PlayerPosY:0,PlayerPos2X:14,PlayerPos2Y:14});
                        if(err) return console.log(err);
                        client.close();
                        console.log("Игра не найдена, создана новая");
                        eventEmitter.emit('GameLoad');
                    });
                }
                if(err) return console.log(err);
                client.close();
            });
        });
        console.log('У пользователя есть cookie', cookie);
    }
});

// Serve the index page 
app.get("/", function (request, response) {
  response.sendFile(__dirname + '/index.html'); 
});

// Serve the assets directory
app.use('/assets',express.static('assets'))

// Listen on port 5000
app.set('port', (process.env.PORT || 5000));
http.listen(app.get('port'), function(){
  console.log('listening on port',app.get('port'));
});

var players = {}; //Keeps a table of all players, the key is the socket id
// Tell Socket.io to start accepting connections
io.on('connection', function(socket){
	// Listen for a new player trying to connect
	socket.on('game',function(state){
		players[socket.id] = state;
        //var a = socket.request.headers.cookie.cookieName;
        //var b = cookie.parse(a); //does not translate
        console.log("game_cookie");
        console.log(socket.request.headers.cookie);
        var cookie1 = cookie.parse(socket.request.headers.cookie);
        var b=cookie1['cookieName'];
        socket.join(socket.id);
        var numGame=findGame(b);
        socket.emit('stateGame',localBD[numGame]);
	});

    // Listen for a disconnection and update our player table
    socket.on('disconnect',function(state){
        console.log("disconnect_cookie");
        console.log(socket.request.headers.cookie);
        if(socket.request.headers.cookie !== undefined){
            var a = socket.request.headers.cookie;
            var cookie1 = cookie.parse(a);
            var b=cookie1['cookieName'];
            var numGame=findGame(b);
            UpdateGameState(b,localBD[numGame]);
            console.log("Куки на выходе")
            console.log(b);
            delete players[socket.id];
        }
    });
    socket.on('move',function(direction){
        var a = socket.request.headers.cookie;
        var cookie1 = cookie.parse(socket.request.headers.cookie);
        var b=cookie1['cookieName'];
        // Broadcast a signal to everyone containing the updated players list
        var numGame=findGame(b);
        if(localBD[numGame]!==undefined){
            switch (direction) {
                case 'w':
                    localBD[numGame].stateGame[localBD[numGame].PlayerPos2X][localBD[numGame].PlayerPos2Y]=2;
                    if(!(localBD[numGame].PlayerPos2X-1<0)){
                        localBD[numGame].PlayerPos2X--;
                    }
                    localBD[numGame].stateGame[localBD[numGame].PlayerPos2X][localBD[numGame].PlayerPos2Y]=4;
                    //UpdateGameState(b.cookieName,localBD[numGame]);
                    socket.emit('stateGame',localBD[numGame]);
                    break;
                case 's':
                    localBD[numGame].stateGame[localBD[numGame].PlayerPos2X][localBD[numGame].PlayerPos2Y]=2;
                    if(!(localBD[numGame].PlayerPos2X+1>14)){
                        localBD[numGame].PlayerPos2X++;
                    }
                    localBD[numGame].stateGame[localBD[numGame].PlayerPos2X][localBD[numGame].PlayerPos2Y]=4;
                    //UpdateGameState(b.cookieName,localBD[numGame]);
                    socket.emit('stateGame',localBD[numGame]);
                    break;
                case 'a':
                    localBD[numGame].stateGame[localBD[numGame].PlayerPos2X][localBD[numGame].PlayerPos2Y]=2;
                    if(!(localBD[numGame].PlayerPos2Y-1<0)){
                        localBD[numGame].PlayerPos2Y--;
                    }
                    localBD[numGame].stateGame[localBD[numGame].PlayerPos2X][localBD[numGame].PlayerPos2Y]=4;
                    //UpdateGameState(b.cookieName,localBD[numGame]);
                    socket.emit('stateGame',localBD[numGame]);
                    break;
                case 'd':
                    localBD[numGame].stateGame[localBD[numGame].PlayerPos2X][localBD[numGame].PlayerPos2Y]=2;
                    if(!(localBD[numGame].PlayerPos2Y+1>14)){
                        localBD[numGame].PlayerPos2Y++;
                    }
                    localBD[numGame].stateGame[localBD[numGame].PlayerPos2X][localBD[numGame].PlayerPos2Y]=4;
                    //UpdateGameState(b.cookieName,localBD[numGame]);
                    socket.emit('stateGame',localBD[numGame]);
                    break;
                default:
            }

        }
    });
});

function findGame(b) {
    for(var i=0;i<localBD.length;i++){
        if(localBD[i].idUser.localeCompare(b)===0) {
            return i;
        }
    }
}
function UpdateGameState(idUser,game) {
    const mongoClient = new MongoClient("mongodb://localhost:27017/", { useNewUrlParser: true });
    if(game!==undefined) {
        mongoClient.connect(function (err, client) {
            if (err) return console.log(err);
            const db = client.db("usersdb");
            db.collection("games").findOneAndUpdate(
                {idUser: idUser},              // критерий выборки
                {
                    $set: {
                        stateGame: game.stateGame,
                        whoGo: game.whoGo,
                        PlayerPosX: game.PlayerPosX,
                        PlayerPosY: game.PlayerPosY,
                        PlayerPos2X: game.PlayerPos2X,
                        PlayerPos2Y: game.PlayerPos2Y
                    }
                },     // параметр обновления
                {                           // доп. опции обновления
                    returnOriginal: false
                },
                function (err, result) {
                    //console.log(result);
                    //client.close();
                }
            );
        });
    }else{
        console.log("Ошибка в обновлении БД, нет обекта game")
    }
}
