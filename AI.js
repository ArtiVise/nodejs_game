"use strict";
/**Алгоритм AI**/
var copyStateGame = new Array(15);
var findStateGame = new Array(15);

var load = function () {
    for (var i = 0; i < copyStateGame.length; i++) {
        copyStateGame[i] = new Array(15);
    }

    for (i = 0; i < findStateGame.length; i++) {
        findStateGame[i] = new Array(15);
    }
};

/**Обработка ответа AI**/
var makeMove = function (game) {
    switch (answerAI(game)) {
        case 'w':
            if (aiCanGo(game.PlayerPosX - 1, game.PlayerPosY, game.stateGame) === 1) {
                game.stateGame[game.PlayerPosX][game.PlayerPosY] = 1;
                game.PlayerPosX--;
                game.stateGame[game.PlayerPosX][game.PlayerPosY] = 3;
            }
            break;
        case 's':
            if (aiCanGo(game.PlayerPosX + 1, game.PlayerPosY, game.stateGame) === 1) {
                game.stateGame[game.PlayerPosX][game.PlayerPosY] = 1;
                game.PlayerPosX++;
                game.stateGame[game.PlayerPosX][game.PlayerPosY] = 3;
            }
            break;
        case 'a':
            if (aiCanGo(game.PlayerPosX, game.PlayerPosY - 1, game.stateGame) === 1) {
                game.stateGame[game.PlayerPosX][game.PlayerPosY] = 1;
                game.PlayerPosY--;
                game.stateGame[game.PlayerPosX][game.PlayerPosY] = 3;
            }
            break;
        case 'd':
            if (aiCanGo(game.PlayerPosX, game.PlayerPosY + 1, game.stateGame) === 1) {
                game.stateGame[game.PlayerPosX][game.PlayerPosY] = 1;
                game.PlayerPosY++;
                game.stateGame[game.PlayerPosX][game.PlayerPosY] = 3;
            }
            break;
    }
    game.step++;
};

/**Simple AI*/

/*
function answerAI(game){
	var canGo0 = whereAiCanGo(game,0);
	var canGo1 = whereAiCanGo(game,1);
	if(canGo0.length!==0){
		return canGo0[getRandomInRange(0,canGo0.length-1)];
	}else{
		return canGo1[getRandomInRange(0,canGo1.length-1)];
	}
}
*/

function answerAI(game) {
    var answer = null;
    for (var i = 0; i < 15; i++) {
        for (var j = 0; j < 15; j++) {
            copyStateGame[i][j] = game.stateGame[i][j];
        }
    }

    /** Борьба за центр **/
    /*if (game.stateGame[7][7] !== 2 && game.stateGame[7][7] !== 4 && game.stateGame[7][7] !== 1 && game.stateGame[7][7] !== 3) {
        if (game.PlayerPosX === 0 && game.PlayerPosY === 0) {
            if (game.PlayerPos2X === 13 && game.PlayerPos2Y === 14) {
                return "d";
            }
            if (game.PlayerPos2X === 14 && game.PlayerPos2Y === 13) {
                return "s";
            }
        }
        if ((game.stateGame[0][1] === 1 || game.stateGame[0][1] === 3 || game.stateGame[7][0] === 1 || game.stateGame[7][0] === 3) && game.PlayerPosY < 7) {
            return "d";
        }
        if ((game.stateGame[1][0] === 1 || game.stateGame[1][0] === 3 || game.stateGame[0][7] === 1 || game.stateGame[0][7] === 3) && game.PlayerPosX < 7) {
            return "s";
        }
    }*/
    if (game.stateGame[4][10] === 0 && game.stateGame[10][4] === 0){//game.stateGame[4][10] !== 1 && game.stateGame[4][10] !== 3 && game.stateGame[10][4] !== 1 && game.stateGame[10][4] !== 3) {
       if (game.PlayerPosX === 0 && game.PlayerPosY === 0) {
           game.strategy = "not";
           if (game.PlayerPos2X === 13 && game.PlayerPos2Y === 14) {
               game.strategy = "right";
               return "d";
           }
           if (game.PlayerPos2X === 14 && game.PlayerPos2Y === 13) {
               game.strategy = "left";
               return "s";
           }
       }
       if (game.strategy === "right") {
           if(game.PlayerPosY < 4){
               return "d";
           } else {
               if (game.stateGame[game.PlayerPosX][game.PlayerPosY + 1] === 0) {
                   return "s";
               }
           }
       }
        if (game.strategy === "left") {
            if(game.PlayerPosX < 4){
                return "s";
            } else {
                if (game.stateGame[game.PlayerPosX + 1][game.PlayerPosY] === 0) {
                    return "d";
                }
            }
        }

    }
    if(game.strategy === "right"){
        if(game.stateGame[10][4] === 3){
            game.strategy = "goWhiteCellNearStartEnemyBelowDiagonal";
            game.goWhiteCellXY = findWhiteCellNearStartEnemy(game, true);
        }
        if(game.stateGame[game.PlayerPosX + 1][game.PlayerPosY] !== 0){
            game.strategy = "goWhiteCellNearStartEnemyAboveDiagonal";
            game.goWhiteCellXY = findWhiteCellNearStartEnemy(game, false);
        }
    }
    if(game.strategy === "left"){
        if (game.stateGame[4][10] === 3) {
            game.strategy = "goWhiteCellNearStartEnemyAboveDiagonal";
            game.goWhiteCellXY = findWhiteCellNearStartEnemy(game, false);
        }
        if(game.stateGame[game.PlayerPosX][game.PlayerPosY + 1] !== 0){
            game.strategy = "goWhiteCellNearStartEnemyBelowDiagonal";
            game.goWhiteCellXY = findWhiteCellNearStartEnemy(game, true);
        }
    }
    if(game.strategy === "goWhiteCellNearStartEnemyBelowDiagonal"){
        if(game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 2 &&
            game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 4) {
            answer = findRouteToWhiteCellXY(game, game.goWhiteCellXY[0], game.goWhiteCellXY[1], "sdaw");
            if (answer !== "not") {
                return answer;
            }
        }
        game.goWhiteCellXY = findWhiteCellNearStartEnemy(game, true);
        if(game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 2 &&
            game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 4) {
            answer = findRouteToWhiteCellXY(game, game.goWhiteCellXY[0], game.goWhiteCellXY[1], "sdaw");
            if (answer !== "not") {
                return answer;
            } else{
                game.goWhiteCellXY = findWhiteCellNearStartEnemy(game, false);
                if(game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 2 &&
                    game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 4) {
                    answer = findRouteToWhiteCellXY(game, game.goWhiteCellXY[0], game.goWhiteCellXY[1], "dsaw");
                    if(answer !== "not"){
                        game.strategy = "goWhiteCellNearStartEnemyAboveDiagonal";
                        return answer;
                    }
                }
            }
        }
        game.strategy = "not"
    }
    if(game.strategy === "goWhiteCellNearStartEnemyAboveDiagonal"){
        if(game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 2 &&
            game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 4) {
            answer = findRouteToWhiteCellXY(game, game.goWhiteCellXY[0], game.goWhiteCellXY[1], "dsaw");
            if(answer !== "not"){
                return answer;
            }
        }
        game.goWhiteCellXY = findWhiteCellNearStartEnemy(game, false);
        if(game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 2 &&
            game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 4) {
            answer = findRouteToWhiteCellXY(game, game.goWhiteCellXY[0], game.goWhiteCellXY[1], "dsaw");
            if(answer !== "not"){
                return answer;
            } else {
                game.goWhiteCellXY = findWhiteCellNearStartEnemy(game, true);
                if(game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 2 &&
                    game.stateGame[game.goWhiteCellXY[0]][game.goWhiteCellXY[1]] !== 4) {
                    answer = findRouteToWhiteCellXY(game, game.goWhiteCellXY[0], game.goWhiteCellXY[1], "sdaw");
                    if (answer !== "not") {
                        game.strategy = "goWhiteCellNearStartEnemyBelowDiagonal";
                        return answer;
                    }
                }
            }
        }
        game.strategy = "not";
    }
    var canGo0 = whereAiCanGo(game, 0);
    if (canGo0.length !== 0) {
        return canGo0[getRandomInRange(0, canGo0.length - 1)];
    } else {
        answer = findRouteToWhiteCell(game);
        if(answer !== "not"){
            return answer;
        }
        var canGo1 = whereAiCanGo(game, 1);
        return canGo1[getRandomInRange(0, canGo1.length - 1)];
    }
}

/**В какую сторону может сходить AI, на определенный тип поля(0-пустой,1-своё поле)**/
function whereAiCanGo(game, typeCell) {
    var str = "";
    if ((game.PlayerPosX + 1) < 15 && game.stateGame[game.PlayerPosX + 1][game.PlayerPosY] === typeCell) str += "s";
    if ((game.PlayerPosY + 1) < 15 && game.stateGame[game.PlayerPosX][game.PlayerPosY + 1] === typeCell) str += "d";
    if ((game.PlayerPosX - 1) > -1 && game.stateGame[game.PlayerPosX - 1][game.PlayerPosY] === typeCell) str += "w";
    if ((game.PlayerPosY - 1) > -1 && game.stateGame[game.PlayerPosX][game.PlayerPosY - 1] === typeCell) str += "a";
    return str;
}

function getRandomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**Проверка возможности хода для AI**/
function aiCanGo(x, y, stateGame) {
    if ((x > -1 && x < 15) && (y > -1 && y < 15) && (stateGame[x][y] === 0 || stateGame[x][y] === 1)) return 1;
    return 0;
}

function findRouteToWhiteCell(game) {
    var currentState = 0;
    for (var i = 0; i < 15; i++) {
        for (var j = 0; j < 15; j++) {
            findStateGame[i][j] = null;
        }
    }
    findStateGame[game.PlayerPosX][game.PlayerPosY] = 0;
    var arrayPointsRoute = [];
    arrayPointsRoute.push([game.PlayerPosX, game.PlayerPosY]);
    while (arrayPointsRoute.length !== 0) {
        var newArrayPoints = [];
        for (i = 0; i < arrayPointsRoute.length; i++) {
            var x = arrayPointsRoute[i][0];
            var y = arrayPointsRoute[i][1];

            if ((x + 1) < 15 && copyStateGame[x + 1][y] === 1) {
                copyStateGame[x + 1][y] = 5;
                findStateGame[x + 1][y] = currentState + 1;
                newArrayPoints.push([x + 1, y]);
            }
            if ((x + 1) < 15 && copyStateGame[x + 1][y] === 0) {
                findStateGame[x + 1][y] = currentState + 1;
                return route(findStateGame, x + 1, y, "sdaw");
            }
            if ((y + 1) < 15 && copyStateGame[x][y + 1] === 1) {
                copyStateGame[x][y + 1] = 5;
                findStateGame[x][y + 1] = currentState + 1;
                newArrayPoints.push([x, y + 1]);
            }
            if ((y + 1) < 15 && copyStateGame[x][y + 1] === 0) {
                findStateGame[x][y + 1] = currentState + 1;
                return route(findStateGame, x, y + 1, "sdaw");
            }
            if ((x - 1) > -1 && copyStateGame[x - 1][y] === 1) {
                copyStateGame[x - 1][y] = 5;
                findStateGame[x - 1][y] = currentState + 1;
                newArrayPoints.push([x - 1, y]);
            }
            if ((x - 1) > -1 && copyStateGame[x - 1][y] === 0) {
                findStateGame[x - 1][y] = currentState + 1;
                return route(findStateGame, x - 1, y, "sdaw");
            }
            if ((y - 1) > -1 && copyStateGame[x][y - 1] === 1) {
                copyStateGame[x][y - 1] = 5;
                findStateGame[x][y - 1] = currentState + 1;
                newArrayPoints.push([x, y - 1]);
            }
            if ((y - 1) > -1 && copyStateGame[x][y - 1] === 0) {
                findStateGame[x][y - 1] = currentState + 1;
                return route(findStateGame, x, y - 1, "sdaw");
            }
        }
        currentState++;
        arrayPointsRoute = newArrayPoints;
    }
    return "not";
}

function findRouteToWhiteCellXY(game,inX,inY, arrayPriority) {
    var currentState = 0;
    for (var i = 0; i < 15; i++) {
        for (var j = 0; j < 15; j++) {
            copyStateGame[i][j] = game.stateGame[i][j];
        }
    }
    for (var i = 0; i < 15; i++) {
        for (var j = 0; j < 15; j++) {
            findStateGame[i][j] = null;
        }
    }
    findStateGame[game.PlayerPosX][game.PlayerPosY] = 0;
    var arrayPointsRoute = [];
    arrayPointsRoute.push([game.PlayerPosX, game.PlayerPosY]);
    while (arrayPointsRoute.length !== 0) {
        var newArrayPoints = [];
        for (i = 0; i < arrayPointsRoute.length; i++) {
            var x = arrayPointsRoute[i][0];
            var y = arrayPointsRoute[i][1];
            if ((x + 1) < 15 && copyStateGame[x + 1][y] === 0 && x + 1 === inX && y === inY) {
                findStateGame[x + 1][y] = currentState + 1;
                return route(findStateGame, x + 1, y, arrayPriority);
            }
            if ((x + 1) < 15 && (copyStateGame[x + 1][y] === 1 || copyStateGame[x + 1][y] === 0)) {
                copyStateGame[x + 1][y] = 5;
                findStateGame[x + 1][y] = currentState + 1;
                newArrayPoints.push([x + 1, y]);
            }
            if ((y + 1) < 15 && copyStateGame[x][y + 1] === 0 && x === inX && y + 1 === inY) {
                findStateGame[x][y + 1] = currentState + 1;
                return route(findStateGame, x, y + 1, arrayPriority);
            }
            if ((y + 1) < 15 && (copyStateGame[x][y + 1] === 1 || copyStateGame[x][y + 1] === 0)) {
                copyStateGame[x][y + 1] = 5;
                findStateGame[x][y + 1] = currentState + 1;
                newArrayPoints.push([x, y + 1]);
            }
            if ((x - 1) > -1 && copyStateGame[x - 1][y] === 0 && x - 1 === inX && y === inY) {
                findStateGame[x - 1][y] = currentState + 1;
                return route(findStateGame, x - 1, y, arrayPriority);
            }
            if ((x - 1) > -1 && (copyStateGame[x - 1][y] === 1 || copyStateGame[x - 1][y] === 0)) {
                copyStateGame[x - 1][y] = 5;
                findStateGame[x - 1][y] = currentState + 1;
                newArrayPoints.push([x - 1, y]);
            }
            if ((y - 1) > -1 && copyStateGame[x][y - 1] === 0 && x === inX && y - 1 === inY) {
                findStateGame[x][y - 1] = currentState + 1;
                return route(findStateGame, x, y - 1, arrayPriority);
            }
            if ((y - 1) > -1 && (copyStateGame[x][y - 1] === 1 || copyStateGame[x][y - 1] === 0)) {
                copyStateGame[x][y - 1] = 5;
                findStateGame[x][y - 1] = currentState + 1;
                newArrayPoints.push([x, y - 1]);
            }

        }
        currentState++;
        arrayPointsRoute = newArrayPoints;
    }
    return "not";
}

function route(findStateGame, x, y, arrayPriority) {
    var currentState = findStateGame[x][y];
    var curX = x;
    var curY = y;
    var str = "";
    while (findStateGame[curX][curY] !== 0) {
        currentState--;
        for(var i=0;i<4;i++){
            if (arrayPriority[i] === "s" && (curX - 1) > -1 && findStateGame[curX - 1][curY] === currentState) {
                str += "s";
                curX = curX - 1;
                break;
            }
            if (arrayPriority[i] === "d" && (curY - 1) > -1 && findStateGame[curX][curY - 1] === currentState) {
                str += "d";
                curY = curY - 1;
                break;
            }
            if (arrayPriority[i] === "a" && (curY + 1) < 15 && findStateGame[curX][curY + 1] === currentState) {
                str += "a";
                curY = curY + 1;
                break;
            }
            if (arrayPriority[i] === "w" && (curX + 1) < 15 && findStateGame[curX + 1][curY] === currentState) {
                str += "w";
                curX = curX + 1;
                break;
            }
        }

    }
    return str[str.length - 1];
}

function findWhiteCellNearStartEnemy(game, belowDiagonal) {
    if (belowDiagonal) {
        for (var i = 0; i < 14; i++) {
            if (game.stateGame[14][i + 1] !== 0) return [14, i];
        }
    } else {
        for (var i = 0; i < 14; i++) {
            if (game.stateGame[i + 1][14] !== 0) return [i, 14];
        }
    }
    return ["not","not"];
}


module.exports = {load, makeMove};